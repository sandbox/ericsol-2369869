<?php
/*
* @file
* This include contains helper functions (mostly queries) for the content queue module
*/

function content_queue_get_all_roles() {

  $items = array();
  $result = db_query('
  SELECT r.rid, r.name
  FROM {role} r
  ORDER BY r.name ASC');
  
  foreach ($result as $row) {
    $items[] = array('rid' => $row->rid, 'name' => $row->name);
  }

  return $items;
}

/**
* Helper function implements EntityFieldQuery
* to get existing node for specific type owned by current user.
* Uses DESC ordering on creation date to get last node if there somehow are multiple
*/
function content_queue_get_existing(&$contenttype, &$content_queue_uid) {
  $existing_nids = array();
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $contenttype)
    ->propertyCondition('uid', $content_queue_uid)
    ->propertyOrderBy('created', 'DESC')
    ->range(0,1);
  $result = $query->execute();
  if (isset($result['node'])) {
    $existing_nids = array_keys($result['node']);
  }
  
  return $existing_nids;
}

function content_queue_get_types(&$content_queue_uid) {
  // first get all content queue types

  $result = db_query('
  SELECT type
  FROM {node_type}
  ');

  $content_queue = array();
  $index = 0;
  foreach ($result as $row) {
    if (variable_get('content_queue_button_' . $row->type) == 1) {
      $content_queue[$index]['type'] = $row->type;
      $content_queue[$index]['weight'] = variable_get('content_queue_weight_' . $row->type);
      $content_queue[$index]['label'] = variable_get('content_queue_label_' . $row->type);
      $index++;
    }
  }
  if ($content_queue_uid) {
    $role_ids = array(0,2);  
  } else {
    $role_ids = array(0); 
  }
  // get roles for user
  $result = db_query('
  SELECT ur.rid
  FROM {users_roles} ur
  WHERE ur.uid = :uid',
  array(':uid' => $content_queue_uid));
  
  // because we use [] other roles get appended to the existing role 0
  foreach ($result as $row) {
    $role_ids[] = intval($row->rid);
  }
  // check if user has bypass or administer content permissions
  $contenttypes = array();
  $bypassroles = array('administer nodes', 'bypass node access');
  $result = db_query('
  SELECT r.permission
  FROM {role_permission} r
  WHERE r.rid IN (:roles) AND r.module = :module AND r.permission IN (:permission)',
  array(':roles' => $role_ids, ':module' => 'node', ':permission' => $bypassroles))->fetchfield();

  if (!empty($result)) {
    $contenttypes = $content_queue;
    return $contenttypes;
  } else {
  // check if user has "create [type]" permissions  
      $index = 0;
      foreach ($content_queue as $content_queuetype) {
        $result = db_query('
        SELECT r.permission
        FROM {role_permission} r
        WHERE r.rid IN (:roles) AND r.module IN (:module) AND r.permission IN (:permission)',
        array(':roles' => $role_ids, ':module' => 'node', ':permission' => 'create ' . $content_queuetype['type'] . ' content'))->fetchField();
      
        if (!empty($result)) {
          $contenttypes[$index]['type'] = $content_queuetype['type'];
          $contenttypes[$index]['weight'] = $content_queuetype['weight'];
          $contenttypes[$index]['label'] = $content_queuetype['label'];
          $index++;
        }
      }        
    }
 
  return $contenttypes;
}


/**
* Function for retrieving start-/endtime for a given entity_id
*
*
*/
function content_queue_field_validity(&$entity_id, $tablename, $fieldname) {
  $items = array();
  $result = db_query('
  SELECT ve.entity_id, ve.'.$fieldname.'
  FROM {'.$tablename.'} ve
  WHERE ve.entity_id = :entity_id',
  array(':entity_id' => $entity_id));
  
  foreach ($result as $row) {
    $items[] = array('entity_id' => $row->entity_id, $fieldname => $row->$fieldname);
  }
 
  return $items;
}